/* im-details.c generated by valac 0.35.3.10-6b27, the Vala compiler
 * generated from im-details.vala, do not modify */

/*
 * Copyright (C) 2011, 2015 Collabora Ltd.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Raul Gutierrez Segales <raul.gutierrez.segales@collabora.co.uk>
 *          Philip Withnall <philip.withnall@collabora.co.uk>
 *
 */

#include <glib.h>
#include <glib-object.h>
#include <eds-test.h>
#include <folks-test.h>
#include <gee.h>
#include <stdlib.h>
#include <string.h>
#include <folks/folks.h>


#define TYPE_IM_DETAILS_TESTS (im_details_tests_get_type ())
#define IM_DETAILS_TESTS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_IM_DETAILS_TESTS, ImDetailsTests))
#define IM_DETAILS_TESTS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_IM_DETAILS_TESTS, ImDetailsTestsClass))
#define IS_IM_DETAILS_TESTS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_IM_DETAILS_TESTS))
#define IS_IM_DETAILS_TESTS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_IM_DETAILS_TESTS))
#define IM_DETAILS_TESTS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_IM_DETAILS_TESTS, ImDetailsTestsClass))

typedef struct _ImDetailsTests ImDetailsTests;
typedef struct _ImDetailsTestsClass ImDetailsTestsClass;
typedef struct _ImDetailsTestsPrivate ImDetailsTestsPrivate;
#define _g_free0(var) (var = (g_free (var), NULL))
#define __vala_GValue_free0(var) ((var == NULL) ? NULL : (var = (_vala_GValue_free (var), NULL)))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
#define _vala_assert(expr, msg) if G_LIKELY (expr) ; else g_assertion_message_expr (G_LOG_DOMAIN, __FILE__, __LINE__, G_STRFUNC, msg);
#define _vala_return_if_fail(expr, msg) if G_LIKELY (expr) ; else { g_return_if_fail_warning (G_LOG_DOMAIN, G_STRFUNC, msg); return; }
#define _vala_return_val_if_fail(expr, msg, val) if G_LIKELY (expr) ; else { g_return_if_fail_warning (G_LOG_DOMAIN, G_STRFUNC, msg); return val; }
#define _vala_warn_if_fail(expr, msg) if G_LIKELY (expr) ; else g_warn_message (G_LOG_DOMAIN, __FILE__, __LINE__, G_STRFUNC, msg);

struct _ImDetailsTests {
	EdsTestTestCase parent_instance;
	ImDetailsTestsPrivate * priv;
};

struct _ImDetailsTestsClass {
	EdsTestTestCaseClass parent_class;
};


static gpointer im_details_tests_parent_class = NULL;

GType im_details_tests_get_type (void) G_GNUC_CONST;
enum  {
	IM_DETAILS_TESTS_DUMMY_PROPERTY
};
ImDetailsTests* im_details_tests_new (void);
ImDetailsTests* im_details_tests_construct (GType object_type);
void im_details_tests_test_im_details_interface (ImDetailsTests* self);
static void _im_details_tests_test_im_details_interface_folks_test_case_test_method (gpointer self);
static GValue* _g_value_dup (GValue* self);
static void _vala_GValue_free (GValue* self);
gint _vala_main (gchar** args, int args_length1);
static void _vala_array_destroy (gpointer array, gint array_length, GDestroyNotify destroy_func);
static void _vala_array_free (gpointer array, gint array_length, GDestroyNotify destroy_func);


static void _im_details_tests_test_im_details_interface_folks_test_case_test_method (gpointer self) {
#line 31 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	im_details_tests_test_im_details_interface ((ImDetailsTests*) self);
#line 84 "im-details.c"
}


ImDetailsTests* im_details_tests_construct (GType object_type) {
	ImDetailsTests * self = NULL;
#line 29 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	self = (ImDetailsTests*) eds_test_test_case_construct (object_type, "ImDetailsTests");
#line 31 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	folks_test_case_add_test ((FolksTestCase*) self, "test im details interface", _im_details_tests_test_im_details_interface_folks_test_case_test_method, g_object_ref (self), g_object_unref);
#line 27 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	return self;
#line 96 "im-details.c"
}


ImDetailsTests* im_details_tests_new (void) {
#line 27 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	return im_details_tests_construct (TYPE_IM_DETAILS_TESTS);
#line 103 "im-details.c"
}


static GValue* _g_value_dup (GValue* self) {
#line 38 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	return g_boxed_copy (G_TYPE_VALUE, self);
#line 110 "im-details.c"
}


static void _vala_GValue_free (GValue* self) {
#line 38 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	g_value_unset (self);
#line 38 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	g_free (self);
#line 119 "im-details.c"
}


static gpointer __g_value_dup0 (gpointer self) {
#line 43 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	return self ? _g_value_dup (self) : NULL;
#line 126 "im-details.c"
}


static gpointer _g_object_ref0 (gpointer self) {
#line 50 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	return self ? g_object_ref (self) : NULL;
#line 133 "im-details.c"
}


void im_details_tests_test_im_details_interface (ImDetailsTests* self) {
	GeeHashMap* c1;
	GeeHashMap* _tmp0_;
	gchar* im_addrs;
	gchar* _tmp1_;
	const gchar* _tmp2_;
	gchar* _tmp3_;
	GValue* v = NULL;
	GValue _tmp4_ = {0};
	GValue _tmp5_;
	GValue* _tmp6_;
	GValue* _tmp7_;
	GeeHashMap* _tmp8_;
	GValue* _tmp9_;
	GValue* _tmp10_;
	GValue _tmp11_ = {0};
	GValue _tmp12_;
	GValue* _tmp13_;
	GValue* _tmp14_;
	const gchar* _tmp15_;
	GeeHashMap* _tmp16_;
	GValue* _tmp17_;
	GValue* _tmp18_;
	EdsTestBackend* _tmp19_;
	GeeHashMap* _tmp20_;
	GeeHashMap* _tmp21_;
	EdsTestBackend* _tmp22_;
	gboolean found_addr_1;
	gboolean found_addr_2;
	FolksIndividualAggregator* aggregator;
	FolksIndividualAggregator* _tmp23_;
	FolksIndividualAggregator* _tmp24_;
	gchar* _tmp25_;
	gchar** _tmp26_;
	gchar** _tmp27_;
	gint _tmp27__length1;
	FolksIndividual* i;
	FolksIndividualAggregator* _tmp28_;
	FolksIndividual* _tmp29_;
	gboolean _tmp56_;
	gboolean _tmp57_;
#line 35 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	g_return_if_fail (self != NULL);
#line 38 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp0_ = gee_hash_map_new (G_TYPE_STRING, (GBoxedCopyFunc) g_strdup, g_free, G_TYPE_VALUE, (GBoxedCopyFunc) _g_value_dup, _vala_GValue_free, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
#line 38 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	c1 = _tmp0_;
#line 39 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp1_ = g_strdup ("im_jabber_home_1#test1@example.org");
#line 39 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	im_addrs = _tmp1_;
#line 40 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp2_ = im_addrs;
#line 40 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp3_ = g_strconcat (_tmp2_, ",im_yahoo_home_1#test2@example.org", NULL);
#line 40 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_g_free0 (im_addrs);
#line 40 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	im_addrs = _tmp3_;
#line 43 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	g_value_init (&_tmp4_, G_TYPE_STRING);
#line 43 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp5_ = _tmp4_;
#line 43 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp6_ = __g_value_dup0 (&_tmp5_);
#line 43 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	__vala_GValue_free0 (v);
#line 43 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	v = _tmp6_;
#line 43 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	G_IS_VALUE (&_tmp5_) ? (g_value_unset (&_tmp5_), NULL) : NULL;
#line 44 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp7_ = v;
#line 44 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	g_value_set_string (_tmp7_, "persona #1");
#line 45 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp8_ = c1;
#line 45 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp9_ = v;
#line 45 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	v = NULL;
#line 45 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp10_ = _tmp9_;
#line 45 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	gee_abstract_map_set ((GeeAbstractMap*) _tmp8_, "full_name", _tmp10_);
#line 45 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	__vala_GValue_free0 (_tmp10_);
#line 46 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	g_value_init (&_tmp11_, G_TYPE_STRING);
#line 46 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp12_ = _tmp11_;
#line 46 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp13_ = __g_value_dup0 (&_tmp12_);
#line 46 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	__vala_GValue_free0 (v);
#line 46 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	v = _tmp13_;
#line 46 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	G_IS_VALUE (&_tmp12_) ? (g_value_unset (&_tmp12_), NULL) : NULL;
#line 47 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp14_ = v;
#line 47 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp15_ = im_addrs;
#line 47 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	g_value_set_string (_tmp14_, _tmp15_);
#line 48 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp16_ = c1;
#line 48 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp17_ = v;
#line 48 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	v = NULL;
#line 48 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp18_ = _tmp17_;
#line 48 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	gee_abstract_map_set ((GeeAbstractMap*) _tmp16_, "im_addresses", _tmp18_);
#line 48 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	__vala_GValue_free0 (_tmp18_);
#line 50 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp19_ = ((EdsTestTestCase*) self)->eds_backend;
#line 50 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp20_ = c1;
#line 50 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp21_ = _g_object_ref0 (_tmp20_);
#line 50 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	eds_test_backend_add_contact (_tmp19_, _tmp21_);
#line 51 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp22_ = ((EdsTestTestCase*) self)->eds_backend;
#line 51 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	eds_test_backend_commit_contacts_to_addressbook_sync (_tmp22_);
#line 54 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	found_addr_1 = FALSE;
#line 55 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	found_addr_2 = FALSE;
#line 59 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp23_ = folks_individual_aggregator_dup ();
#line 59 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	aggregator = _tmp23_;
#line 60 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp24_ = aggregator;
#line 60 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp25_ = g_strdup ("persona #1");
#line 60 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp26_ = g_new0 (gchar*, 1 + 1);
#line 60 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp26_[0] = _tmp25_;
#line 60 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp27_ = _tmp26_;
#line 60 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp27__length1 = 1;
#line 60 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	folks_test_utils_aggregator_prepare_and_wait_for_individuals_sync_with_timeout (_tmp24_, _tmp27_, 1, 5);
#line 60 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp27_ = (_vala_array_free (_tmp27_, _tmp27__length1, (GDestroyNotify) g_free), NULL);
#line 64 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp28_ = aggregator;
#line 64 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp29_ = folks_test_utils_get_individual_by_name (_tmp28_, "persona #1");
#line 64 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	i = _tmp29_;
#line 296 "im-details.c"
	{
		GeeIterator* _proto_it;
		FolksIndividual* _tmp30_;
		GeeMultiMap* _tmp31_;
		GeeMultiMap* _tmp32_;
		GeeSet* _tmp33_;
		GeeSet* _tmp34_;
		GeeIterator* _tmp35_;
		GeeIterator* _tmp36_;
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
		_tmp30_ = i;
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
		_tmp31_ = folks_im_details_get_im_addresses ((FolksImDetails*) _tmp30_);
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
		_tmp32_ = _tmp31_;
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
		_tmp33_ = gee_multi_map_get_keys (_tmp32_);
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
		_tmp34_ = _tmp33_;
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
		_tmp35_ = gee_iterable_iterator ((GeeIterable*) _tmp34_);
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
		_tmp36_ = _tmp35_;
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
		_g_object_unref0 (_tmp34_);
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
		_proto_it = _tmp36_;
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
		while (TRUE) {
#line 326 "im-details.c"
			GeeIterator* _tmp37_;
			gboolean _tmp38_;
			gchar* proto;
			GeeIterator* _tmp39_;
			gpointer _tmp40_;
			GeeCollection* addrs;
			FolksIndividual* _tmp41_;
			GeeMultiMap* _tmp42_;
			GeeMultiMap* _tmp43_;
			const gchar* _tmp44_;
			GeeCollection* _tmp45_;
			const gchar* _tmp46_;
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			_tmp37_ = _proto_it;
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			_tmp38_ = gee_iterator_next (_tmp37_);
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			if (!_tmp38_) {
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
				break;
#line 347 "im-details.c"
			}
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			_tmp39_ = _proto_it;
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			_tmp40_ = gee_iterator_get (_tmp39_);
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			proto = (gchar*) _tmp40_;
#line 67 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			_tmp41_ = i;
#line 67 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			_tmp42_ = folks_im_details_get_im_addresses ((FolksImDetails*) _tmp41_);
#line 67 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			_tmp43_ = _tmp42_;
#line 67 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			_tmp44_ = proto;
#line 67 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			_tmp45_ = gee_multi_map_get (_tmp43_, _tmp44_);
#line 67 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			addrs = _tmp45_;
#line 69 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			_tmp46_ = proto;
#line 69 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			if (g_strcmp0 (_tmp46_, "jabber") == 0) {
#line 371 "im-details.c"
				GeeCollection* _tmp47_;
				FolksImFieldDetails* _tmp48_;
				FolksImFieldDetails* _tmp49_;
				gboolean _tmp50_;
#line 71 "/opt/gnome/source/folks/tests/eds/im-details.vala"
				_tmp47_ = addrs;
#line 71 "/opt/gnome/source/folks/tests/eds/im-details.vala"
				_tmp48_ = folks_im_field_details_new ("test1@example.org", NULL);
#line 71 "/opt/gnome/source/folks/tests/eds/im-details.vala"
				_tmp49_ = _tmp48_;
#line 71 "/opt/gnome/source/folks/tests/eds/im-details.vala"
				_tmp50_ = gee_collection_contains (_tmp47_, _tmp49_);
#line 71 "/opt/gnome/source/folks/tests/eds/im-details.vala"
				found_addr_1 = _tmp50_;
#line 71 "/opt/gnome/source/folks/tests/eds/im-details.vala"
				_g_object_unref0 (_tmp49_);
#line 388 "im-details.c"
			} else {
				const gchar* _tmp51_;
#line 74 "/opt/gnome/source/folks/tests/eds/im-details.vala"
				_tmp51_ = proto;
#line 74 "/opt/gnome/source/folks/tests/eds/im-details.vala"
				if (g_strcmp0 (_tmp51_, "yahoo") == 0) {
#line 395 "im-details.c"
					GeeCollection* _tmp52_;
					FolksImFieldDetails* _tmp53_;
					FolksImFieldDetails* _tmp54_;
					gboolean _tmp55_;
#line 76 "/opt/gnome/source/folks/tests/eds/im-details.vala"
					_tmp52_ = addrs;
#line 76 "/opt/gnome/source/folks/tests/eds/im-details.vala"
					_tmp53_ = folks_im_field_details_new ("test2@example.org", NULL);
#line 76 "/opt/gnome/source/folks/tests/eds/im-details.vala"
					_tmp54_ = _tmp53_;
#line 76 "/opt/gnome/source/folks/tests/eds/im-details.vala"
					_tmp55_ = gee_collection_contains (_tmp52_, _tmp54_);
#line 76 "/opt/gnome/source/folks/tests/eds/im-details.vala"
					found_addr_2 = _tmp55_;
#line 76 "/opt/gnome/source/folks/tests/eds/im-details.vala"
					_g_object_unref0 (_tmp54_);
#line 412 "im-details.c"
				}
			}
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			_g_object_unref0 (addrs);
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
			_g_free0 (proto);
#line 419 "im-details.c"
		}
#line 65 "/opt/gnome/source/folks/tests/eds/im-details.vala"
		_g_object_unref0 (_proto_it);
#line 423 "im-details.c"
	}
#line 81 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp56_ = found_addr_1;
#line 81 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_vala_assert (_tmp56_, "found_addr_1");
#line 82 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp57_ = found_addr_2;
#line 82 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_vala_assert (_tmp57_, "found_addr_2");
#line 35 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_g_object_unref0 (i);
#line 35 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_g_object_unref0 (aggregator);
#line 35 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	__vala_GValue_free0 (v);
#line 35 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_g_free0 (im_addrs);
#line 35 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_g_object_unref0 (c1);
#line 443 "im-details.c"
}


static void im_details_tests_class_init (ImDetailsTestsClass * klass) {
#line 25 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	im_details_tests_parent_class = g_type_class_peek_parent (klass);
#line 450 "im-details.c"
}


static void im_details_tests_instance_init (ImDetailsTests * self) {
}


GType im_details_tests_get_type (void) {
	static volatile gsize im_details_tests_type_id__volatile = 0;
	if (g_once_init_enter (&im_details_tests_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (ImDetailsTestsClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) im_details_tests_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (ImDetailsTests), 0, (GInstanceInitFunc) im_details_tests_instance_init, NULL };
		GType im_details_tests_type_id;
		im_details_tests_type_id = g_type_register_static (EDS_TEST_TYPE_TEST_CASE, "ImDetailsTests", &g_define_type_info, 0);
		g_once_init_leave (&im_details_tests_type_id__volatile, im_details_tests_type_id);
	}
	return im_details_tests_type_id__volatile;
}


gint _vala_main (gchar** args, int args_length1) {
	gint result = 0;
	ImDetailsTests* tests;
	ImDetailsTests* _tmp0_;
#line 88 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	g_test_init (&args_length1, &args, NULL);
#line 90 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_tmp0_ = im_details_tests_new ();
#line 90 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	tests = _tmp0_;
#line 91 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	folks_test_case_register ((FolksTestCase*) tests);
#line 92 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	g_test_run ();
#line 93 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	folks_test_case_final_tear_down ((FolksTestCase*) tests);
#line 95 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	result = 0;
#line 95 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	_g_object_unref0 (tests);
#line 95 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	return result;
#line 492 "im-details.c"
}


int main (int argc, char ** argv) {
#if !GLIB_CHECK_VERSION (2,35,0)
	g_type_init ();
#endif
#line 86 "/opt/gnome/source/folks/tests/eds/im-details.vala"
	return _vala_main (argv, argc);
#line 502 "im-details.c"
}


static void _vala_array_destroy (gpointer array, gint array_length, GDestroyNotify destroy_func) {
	if ((array != NULL) && (destroy_func != NULL)) {
		int i;
		for (i = 0; i < array_length; i = i + 1) {
			if (((gpointer*) array)[i] != NULL) {
				destroy_func (((gpointer*) array)[i]);
			}
		}
	}
}


static void _vala_array_free (gpointer array, gint array_length, GDestroyNotify destroy_func) {
	_vala_array_destroy (array, array_length, destroy_func);
	g_free (array);
}



