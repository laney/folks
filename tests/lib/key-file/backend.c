/* backend.c generated by valac 0.35.3.10-6b27, the Vala compiler
 * generated from backend.vala, do not modify */

/* backend.vala
 *
 * Copyright © 2010 Collabora Ltd.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 *      Philip Withnall <philip.withnall@collabora.co.uk>
 */

#include <glib.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <glib/gstdio.h>
#include <unistd.h>
#include <gobject/gvaluecollector.h>


#define KF_TEST_TYPE_BACKEND (kf_test_backend_get_type ())
#define KF_TEST_BACKEND(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), KF_TEST_TYPE_BACKEND, KfTestBackend))
#define KF_TEST_BACKEND_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), KF_TEST_TYPE_BACKEND, KfTestBackendClass))
#define KF_TEST_IS_BACKEND(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), KF_TEST_TYPE_BACKEND))
#define KF_TEST_IS_BACKEND_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), KF_TEST_TYPE_BACKEND))
#define KF_TEST_BACKEND_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), KF_TEST_TYPE_BACKEND, KfTestBackendClass))

typedef struct _KfTestBackend KfTestBackend;
typedef struct _KfTestBackendClass KfTestBackendClass;
typedef struct _KfTestBackendPrivate KfTestBackendPrivate;
#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_error_free0(var) ((var == NULL) ? NULL : (var = (g_error_free (var), NULL)))
#define _g_io_channel_unref0(var) ((var == NULL) ? NULL : (var = (g_io_channel_unref (var), NULL)))
typedef struct _KfTestParamSpecBackend KfTestParamSpecBackend;

struct _KfTestBackend {
	GTypeInstance parent_instance;
	volatile int ref_count;
	KfTestBackendPrivate * priv;
};

struct _KfTestBackendClass {
	GTypeClass parent_class;
	void (*finalize) (KfTestBackend *self);
};

struct _KfTestBackendPrivate {
	gchar* key_file_name;
};

struct _KfTestParamSpecBackend {
	GParamSpec parent_instance;
};


static gpointer kf_test_backend_parent_class = NULL;

gpointer kf_test_backend_ref (gpointer instance);
void kf_test_backend_unref (gpointer instance);
GParamSpec* kf_test_param_spec_backend (const gchar* name, const gchar* nick, const gchar* blurb, GType object_type, GParamFlags flags);
void kf_test_value_set_backend (GValue* value, gpointer v_object);
void kf_test_value_take_backend (GValue* value, gpointer v_object);
gpointer kf_test_value_get_backend (const GValue* value);
GType kf_test_backend_get_type (void) G_GNUC_CONST;
#define KF_TEST_BACKEND_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), KF_TEST_TYPE_BACKEND, KfTestBackendPrivate))
enum  {
	KF_TEST_BACKEND_DUMMY_PROPERTY
};
void kf_test_backend_set_up (KfTestBackend* self, const gchar* key_file_contents);
void kf_test_backend_tear_down (KfTestBackend* self);
KfTestBackend* kf_test_backend_new (void);
KfTestBackend* kf_test_backend_construct (GType object_type);
static void kf_test_backend_finalize (KfTestBackend * obj);


void kf_test_backend_set_up (KfTestBackend* self, const gchar* key_file_contents) {
	gint fd = 0;
	GIOChannel* channel;
	GIOChannel* _tmp4_;
	const gchar* _tmp11_;
	GError * _inner_error_ = NULL;
#line 27 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	g_return_if_fail (self != NULL);
#line 27 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	g_return_if_fail (key_file_contents != NULL);
#line 101 "backend.c"
	{
		gint _tmp0_;
		gchar* _tmp1_ = NULL;
		gint _tmp2_;
#line 34 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_tmp2_ = g_file_open_tmp ("folks-kf-test-XXXXXX", &_tmp1_, &_inner_error_);
#line 34 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_g_free0 (self->priv->key_file_name);
#line 34 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		self->priv->key_file_name = _tmp1_;
#line 34 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_tmp0_ = _tmp2_;
#line 34 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 34 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			if (_inner_error_->domain == G_FILE_ERROR) {
#line 118 "backend.c"
				goto __catch0_g_file_error;
			}
#line 34 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
#line 34 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			g_clear_error (&_inner_error_);
#line 34 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			return;
#line 127 "backend.c"
		}
#line 34 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		fd = _tmp0_;
#line 131 "backend.c"
	}
	goto __finally0;
	__catch0_g_file_error:
	{
		GError* e = NULL;
		const gchar* _tmp3_;
#line 32 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		e = _inner_error_;
#line 32 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_inner_error_ = NULL;
#line 39 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_tmp3_ = e->message;
#line 39 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_error ("backend.vala:39: Error opening temporary file: %s", _tmp3_);
#line 32 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_g_error_free0 (e);
#line 148 "backend.c"
	}
	__finally0:
#line 32 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 32 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
#line 32 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_clear_error (&_inner_error_);
#line 32 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		return;
#line 159 "backend.c"
	}
#line 43 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	_tmp4_ = g_io_channel_unix_new (fd);
#line 43 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	channel = _tmp4_;
#line 165 "backend.c"
	{
		const gchar* _tmp5_;
#line 46 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_tmp5_ = key_file_contents;
#line 46 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_io_channel_write_chars (channel, (gchar*) _tmp5_, -1, NULL, &_inner_error_);
#line 46 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 46 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			if (_inner_error_->domain == G_CONVERT_ERROR) {
#line 176 "backend.c"
				goto __catch1_g_convert_error;
			}
#line 46 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			if (_inner_error_->domain == G_IO_CHANNEL_ERROR) {
#line 181 "backend.c"
				goto __catch1_g_io_channel_error;
			}
#line 46 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			_g_io_channel_unref0 (channel);
#line 46 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
#line 46 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			g_clear_error (&_inner_error_);
#line 46 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			return;
#line 192 "backend.c"
		}
	}
	goto __finally1;
	__catch1_g_convert_error:
	{
		GError* e = NULL;
		const gchar* _tmp6_;
		const gchar* _tmp7_;
		const gchar* _tmp8_;
#line 44 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		e = _inner_error_;
#line 44 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_inner_error_ = NULL;
#line 50 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_tmp6_ = self->priv->key_file_name;
#line 50 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_tmp7_ = e->message;
#line 50 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_tmp8_ = key_file_contents;
#line 50 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_error ("backend.vala:50: Error converting for writing to temporary file '%s': " \
"%s\n" \
"%s", _tmp6_, _tmp7_, _tmp8_);
#line 44 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_g_error_free0 (e);
#line 216 "backend.c"
	}
	goto __finally1;
	__catch1_g_io_channel_error:
	{
		GError* e = NULL;
		const gchar* _tmp9_;
		const gchar* _tmp10_;
#line 44 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		e = _inner_error_;
#line 44 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_inner_error_ = NULL;
#line 55 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_tmp9_ = self->priv->key_file_name;
#line 55 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_tmp10_ = e->message;
#line 55 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_error ("backend.vala:55: Error writing to temporary file '%s': %s", _tmp9_, _tmp10_);
#line 44 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_g_error_free0 (e);
#line 236 "backend.c"
	}
	__finally1:
#line 44 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 44 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_g_io_channel_unref0 (channel);
#line 44 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
#line 44 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_clear_error (&_inner_error_);
#line 44 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		return;
#line 249 "backend.c"
	}
	{
#line 61 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_io_channel_shutdown (channel, TRUE, &_inner_error_);
#line 61 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 61 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			if (_inner_error_->domain == G_IO_CHANNEL_ERROR) {
#line 258 "backend.c"
				goto __catch2_g_io_channel_error;
			}
#line 61 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			_g_io_channel_unref0 (channel);
#line 61 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
#line 61 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			g_clear_error (&_inner_error_);
#line 61 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			return;
#line 269 "backend.c"
		}
	}
	goto __finally2;
	__catch2_g_io_channel_error:
	{
		GError* e = NULL;
#line 59 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		e = _inner_error_;
#line 59 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_inner_error_ = NULL;
#line 59 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_g_error_free0 (e);
#line 282 "backend.c"
	}
	__finally2:
#line 59 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 59 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_g_io_channel_unref0 (channel);
#line 59 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
#line 59 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_clear_error (&_inner_error_);
#line 59 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		return;
#line 295 "backend.c"
	}
#line 64 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	close (fd);
#line 68 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	_tmp11_ = self->priv->key_file_name;
#line 68 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	g_setenv ("FOLKS_BACKEND_KEY_FILE_PATH", _tmp11_, TRUE);
#line 27 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	_g_io_channel_unref0 (channel);
#line 305 "backend.c"
}


void kf_test_backend_tear_down (KfTestBackend* self) {
	const gchar* _tmp0_;
#line 72 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	g_return_if_fail (self != NULL);
#line 75 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	_tmp0_ = self->priv->key_file_name;
#line 75 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (_tmp0_ != NULL) {
#line 317 "backend.c"
		const gchar* _tmp1_;
#line 76 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		_tmp1_ = self->priv->key_file_name;
#line 76 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_remove (_tmp1_);
#line 323 "backend.c"
	}
#line 77 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	_g_free0 (self->priv->key_file_name);
#line 77 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	self->priv->key_file_name = NULL;
#line 329 "backend.c"
}


KfTestBackend* kf_test_backend_construct (GType object_type) {
	KfTestBackend* self = NULL;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	self = (KfTestBackend*) g_type_create_instance (object_type);
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	return self;
#line 339 "backend.c"
}


KfTestBackend* kf_test_backend_new (void) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	return kf_test_backend_construct (KF_TEST_TYPE_BACKEND);
#line 346 "backend.c"
}


static void kf_test_value_backend_init (GValue* value) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	value->data[0].v_pointer = NULL;
#line 353 "backend.c"
}


static void kf_test_value_backend_free_value (GValue* value) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (value->data[0].v_pointer) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		kf_test_backend_unref (value->data[0].v_pointer);
#line 362 "backend.c"
	}
}


static void kf_test_value_backend_copy_value (const GValue* src_value, GValue* dest_value) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (src_value->data[0].v_pointer) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		dest_value->data[0].v_pointer = kf_test_backend_ref (src_value->data[0].v_pointer);
#line 372 "backend.c"
	} else {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		dest_value->data[0].v_pointer = NULL;
#line 376 "backend.c"
	}
}


static gpointer kf_test_value_backend_peek_pointer (const GValue* value) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	return value->data[0].v_pointer;
#line 384 "backend.c"
}


static gchar* kf_test_value_backend_collect_value (GValue* value, guint n_collect_values, GTypeCValue* collect_values, guint collect_flags) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (collect_values[0].v_pointer) {
#line 391 "backend.c"
		KfTestBackend * object;
		object = collect_values[0].v_pointer;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		if (object->parent_instance.g_class == NULL) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			return g_strconcat ("invalid unclassed object pointer for value type `", G_VALUE_TYPE_NAME (value), "'", NULL);
#line 398 "backend.c"
		} else if (!g_value_type_compatible (G_TYPE_FROM_INSTANCE (object), G_VALUE_TYPE (value))) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
			return g_strconcat ("invalid object type `", g_type_name (G_TYPE_FROM_INSTANCE (object)), "' for value type `", G_VALUE_TYPE_NAME (value), "'", NULL);
#line 402 "backend.c"
		}
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		value->data[0].v_pointer = kf_test_backend_ref (object);
#line 406 "backend.c"
	} else {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		value->data[0].v_pointer = NULL;
#line 410 "backend.c"
	}
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	return NULL;
#line 414 "backend.c"
}


static gchar* kf_test_value_backend_lcopy_value (const GValue* value, guint n_collect_values, GTypeCValue* collect_values, guint collect_flags) {
	KfTestBackend ** object_p;
	object_p = collect_values[0].v_pointer;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (!object_p) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		return g_strdup_printf ("value location for `%s' passed as NULL", G_VALUE_TYPE_NAME (value));
#line 425 "backend.c"
	}
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (!value->data[0].v_pointer) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		*object_p = NULL;
#line 431 "backend.c"
	} else if (collect_flags & G_VALUE_NOCOPY_CONTENTS) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		*object_p = value->data[0].v_pointer;
#line 435 "backend.c"
	} else {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		*object_p = kf_test_backend_ref (value->data[0].v_pointer);
#line 439 "backend.c"
	}
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	return NULL;
#line 443 "backend.c"
}


GParamSpec* kf_test_param_spec_backend (const gchar* name, const gchar* nick, const gchar* blurb, GType object_type, GParamFlags flags) {
	KfTestParamSpecBackend* spec;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	g_return_val_if_fail (g_type_is_a (object_type, KF_TEST_TYPE_BACKEND), NULL);
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	spec = g_param_spec_internal (G_TYPE_PARAM_OBJECT, name, nick, blurb, flags);
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	G_PARAM_SPEC (spec)->value_type = object_type;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	return G_PARAM_SPEC (spec);
#line 457 "backend.c"
}


gpointer kf_test_value_get_backend (const GValue* value) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	g_return_val_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, KF_TEST_TYPE_BACKEND), NULL);
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	return value->data[0].v_pointer;
#line 466 "backend.c"
}


void kf_test_value_set_backend (GValue* value, gpointer v_object) {
	KfTestBackend * old;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	g_return_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, KF_TEST_TYPE_BACKEND));
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	old = value->data[0].v_pointer;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (v_object) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_return_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (v_object, KF_TEST_TYPE_BACKEND));
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_return_if_fail (g_value_type_compatible (G_TYPE_FROM_INSTANCE (v_object), G_VALUE_TYPE (value)));
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		value->data[0].v_pointer = v_object;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		kf_test_backend_ref (value->data[0].v_pointer);
#line 486 "backend.c"
	} else {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		value->data[0].v_pointer = NULL;
#line 490 "backend.c"
	}
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (old) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		kf_test_backend_unref (old);
#line 496 "backend.c"
	}
}


void kf_test_value_take_backend (GValue* value, gpointer v_object) {
	KfTestBackend * old;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	g_return_if_fail (G_TYPE_CHECK_VALUE_TYPE (value, KF_TEST_TYPE_BACKEND));
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	old = value->data[0].v_pointer;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (v_object) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_return_if_fail (G_TYPE_CHECK_INSTANCE_TYPE (v_object, KF_TEST_TYPE_BACKEND));
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_return_if_fail (g_value_type_compatible (G_TYPE_FROM_INSTANCE (v_object), G_VALUE_TYPE (value)));
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		value->data[0].v_pointer = v_object;
#line 515 "backend.c"
	} else {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		value->data[0].v_pointer = NULL;
#line 519 "backend.c"
	}
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (old) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		kf_test_backend_unref (old);
#line 525 "backend.c"
	}
}


static void kf_test_backend_class_init (KfTestBackendClass * klass) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	kf_test_backend_parent_class = g_type_class_peek_parent (klass);
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	((KfTestBackendClass *) klass)->finalize = kf_test_backend_finalize;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	g_type_class_add_private (klass, sizeof (KfTestBackendPrivate));
#line 537 "backend.c"
}


static void kf_test_backend_instance_init (KfTestBackend * self) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	self->priv = KF_TEST_BACKEND_GET_PRIVATE (self);
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	self->ref_count = 1;
#line 546 "backend.c"
}


static void kf_test_backend_finalize (KfTestBackend * obj) {
	KfTestBackend * self;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, KF_TEST_TYPE_BACKEND, KfTestBackend);
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	g_signal_handlers_destroy (self);
#line 25 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	_g_free0 (self->priv->key_file_name);
#line 558 "backend.c"
}


GType kf_test_backend_get_type (void) {
	static volatile gsize kf_test_backend_type_id__volatile = 0;
	if (g_once_init_enter (&kf_test_backend_type_id__volatile)) {
		static const GTypeValueTable g_define_type_value_table = { kf_test_value_backend_init, kf_test_value_backend_free_value, kf_test_value_backend_copy_value, kf_test_value_backend_peek_pointer, "p", kf_test_value_backend_collect_value, "p", kf_test_value_backend_lcopy_value };
		static const GTypeInfo g_define_type_info = { sizeof (KfTestBackendClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) kf_test_backend_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (KfTestBackend), 0, (GInstanceInitFunc) kf_test_backend_instance_init, &g_define_type_value_table };
		static const GTypeFundamentalInfo g_define_type_fundamental_info = { (G_TYPE_FLAG_CLASSED | G_TYPE_FLAG_INSTANTIATABLE | G_TYPE_FLAG_DERIVABLE | G_TYPE_FLAG_DEEP_DERIVABLE) };
		GType kf_test_backend_type_id;
		kf_test_backend_type_id = g_type_register_fundamental (g_type_fundamental_next (), "KfTestBackend", &g_define_type_info, &g_define_type_fundamental_info, 0);
		g_once_init_leave (&kf_test_backend_type_id__volatile, kf_test_backend_type_id);
	}
	return kf_test_backend_type_id__volatile;
}


gpointer kf_test_backend_ref (gpointer instance) {
	KfTestBackend * self;
	self = instance;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	g_atomic_int_inc (&self->ref_count);
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	return instance;
#line 583 "backend.c"
}


void kf_test_backend_unref (gpointer instance) {
	KfTestBackend * self;
	self = instance;
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
	if (g_atomic_int_dec_and_test (&self->ref_count)) {
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		KF_TEST_BACKEND_GET_CLASS (self)->finalize (self);
#line 23 "/opt/gnome/source/folks/tests/lib/key-file/backend.vala"
		g_type_free_instance ((GTypeInstance *) self);
#line 596 "backend.c"
	}
}



